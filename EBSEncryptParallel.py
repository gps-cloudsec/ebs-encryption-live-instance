import time
import boto3
import sys
from botocore.exceptions import WaiterError
from botocore.exceptions import ClientError
from botocore.parsers import ResponseParserError
import argparse
import multiprocessing

# list of roots and instance codes
instance_exit_states = [0, 32, 48, 80]
INSTANCE_RUNNING = 16


def main(passed_id, in_id, customer_master_key, profile):

    volume_client = boto3.client('ec2')
    if profile:
        volume_session = boto3.session.Session(profile_name=profile)
    else: 
        volume_session = boto3.session.Session()

    volume_waiter_instance_exists = volume_client.get_waiter('instance_exists')
    volume_waiter_instance_stopped = volume_client.get_waiter('instance_stopped')
    volume_waiter_instance_running = volume_client.get_waiter('instance_running')
    volume_waiter_snapshot_complete = volume_client.get_waiter('snapshot_completed')
    volume_waiter_volume_available = volume_client.get_waiter('volume_available')

    temp_ec2_ref = boto3.resource('ec2')
    temp_ec2 = temp_ec2_ref.Instance(str(in_id))
    passed_volume = temp_ec2_ref.Volume(str(passed_id))
    # passed_volume = ec2.Volume(str(passed_id))
    print("Volume {}".format(passed_volume.id))

    # get the device associated with the current volume
    device = passed_volume.attachments[0]['Device']
    vol_encrypted = passed_volume.encrypted

    # exit if current volume is already encrypted
    if vol_encrypted:
        print("---Volume ({}) is already encrypted---".format(passed_volume.id))
    else:
        # if the current volume is a root volume, stop the ec2 instance
        if device == temp_ec2.root_device_name:
            root_volume_wait(temp_ec2, temp_ec2.id, volume_waiter_instance_stopped)

        # get the current volume's 'DeleteOnTermination' value
        mappings = {'DeleteOnTermination': passed_volume.attachments[0]['DeleteOnTermination']}

        snapshot = None
        snapshot_created = False
        # create a snapshot for the current volume
        print("---Creating Snapshot for Volume ({})---".format(passed_volume.id))
        while not snapshot_created:
            try:
                snapshot = temp_ec2_ref.create_snapshot(VolumeId=passed_volume.id,
                                                        Description='Snapshot of volume {}'.format(passed_volume.id))
                snapshot_created = True
            except ClientError as c:
                # to much traffic for creating snapshots, try again
                print("---Trouble creating snapshot. Waiting---")
                time.sleep(15)
            except ResponseParserError as c:
                # received empty xml from create_snapshot response, try again
                print("---Trouble creating snapshot. Waiting---")
                time.sleep(15)

        # wait until the snapshot is available
        snapshot_complete_wait(snapshot, volume_waiter_snapshot_complete)

        # create a encrypted snapshot dictionary with the master key if provided
        if customer_master_key:
            snapshot_enc_dict = snapshot.copy(SourceRegion=volume_session.region_name,
                                              Description='Encrypted copy of snapshot {}'.format(snapshot.id),
                                              KmsKeyId=customer_master_key, Encrypted=True, )
        else:
            snapshot_enc_dict = snapshot.copy(SourceRegion=volume_session.region_name,
                                              Description='Encrypt copy of snapshot {}'.format(snapshot.id),
                                              Encrypted=True)

        # create the new encrypted snapshot
        print("---Creating Encrypted Snapshot for ({})---".format(snapshot.id))
        snapshot_created = False
        snapshot_enc = None
        while not snapshot_created:
            try:
                snapshot_enc = temp_ec2_ref.Snapshot(snapshot_enc_dict['SnapshotId'])
                snapshot_created = True
            except ClientError as z:
                # to much traffic for creating snapshots, try again
                print("---Trouble creating encrypted snapshot. Waiting---")
                time.sleep(15)
            except ResponseParserError as z:
                # received empty xml for Snapshot response, try again
                print("---Trouble creating encryped snapshot. Waiting---")
                time.sleep(15)

        # wait for the encrypted snapshot to be created
        snapshot_complete_wait(snapshot_enc, volume_waiter_snapshot_complete)

        # create a new encrypted volume from the encrypted snapshot
        print("---Creating new encrypted volume for instance {}".format(temp_ec2.id))
        volume_enc = temp_ec2_ref.create_volume(SnapshotId=snapshot_enc.id,
                                                AvailabilityZone=temp_ec2.placement['AvailabilityZone'])

        # detach the current volume associated the the device on the current instance
        print("---Detaching current volume {} from instance {}".format(passed_volume.id, temp_ec2.id))
        temp_ec2.detach_volume(VolumeId=passed_volume.id, Device=device)

        # if the current volume is not a root volume, wait until it is detached from the running instance
        if device != temp_ec2.root_device_name:
            completed = False
            while not completed:
                try:
                    volume_waiter_volume_available.wait(VolumeIds=[passed_volume.id])
                    completed = True
                except WaiterError as e:
                    print('---ERROR: {}--- Trying Again'.format(e))

        # wait until the new encrypted volume is available
        completed = False
        while not completed:
            try:
                volume_waiter_volume_available.wait(VolumeIds=[volume_enc.id])
                completed = True
            except WaiterError as e:
                print('---ERROR: {}--- Trying Again'.format(e))

        # attach the encrypted volume to the instance on the same device
        print("---Attaching new Encrypted Volume ({}) to Instance ({})".format(volume_enc.id, temp_ec2.id))
        temp_ec2.attach_volume(VolumeId=volume_enc.id, Device=device)

        # modify the mapping on the new encrypted volume
        temp_ec2.modify_attribute(BlockDeviceMappings=[{
            'DeviceName': device,
            'Ebs': {
                'DeleteOnTermination': mappings['DeleteOnTermination']
            }
        }])

        # if the volume is a root volume, restart the instance
        if device == temp_ec2.root_device_name:
            instance_start(temp_ec2, temp_ec2.id, volume_waiter_instance_running)

        # delete snapshots and old volume
        snapshot.delete()
        snapshot_enc.delete()
        passed_volume.delete()


# waits for a snapshot to be available
def snapshot_complete_wait(snapshot, waiter_snapshot_complete):
    completed = False
    while not completed:
        try:
            waiter_snapshot_complete.wait(SnapshotIds=[snapshot.id], WaiterConfig={
                'Delay': 15,
                'MaxAttempts': 123})
            completed = True
        except WaiterError as b:
            # max attempts for waiting reached, try again
            print('---ERROR: {}---'.format(b))
        except ResponseParserError:
            # received empty xml from waiter, try again
            print("Waiting for SnapShot ({}) to finish".format(snapshot.id))


# start an instance if a root volume was encrypted
def instance_start(wait_instance, wait_instance_id, wait_instance_running):
    print("---Restarting Instance ({})---".format(wait_instance_id))
    # wait for the instance to run
    completed = False
    while not completed:
        try:
            wait_instance.start()
            wait_instance_running.wait(InstanceIds=[wait_instance_id])
            completed = True
        except WaiterError as a:
            print('ERROR: {}'.format(a))
        except ResponseParserError:
            print("---Still waiting for Instance ({}) to start---".format(wait_instance_id))


# wait for an instance to stop if the current volume is a root volume
def root_volume_wait(wait_instance, wait_instance_id, wait_instance_stopped):
    # check if the current instance is running, throw error otherwise
    if wait_instance.state['Code'] in instance_exit_states:
        sys.exit('---ERROR: Instance ({}) is \'{}\', please make instance \'active\'---'.format(wait_instance_id,
                                                                                                wait_instance.state[
                                                                                                    'Name']))
    completed = False
    print("---Stopping Instance ({})---".format(wait_instance_id))
    if wait_instance.state['Code'] == INSTANCE_RUNNING:
        while not completed:
            try:
                wait_instance.stop()
                completed = True
            except ResponseParserError:
                print("XML Response Error. Trying to stop Instance")
                time.sleep(15)
    # wait until the instance is stopped
    try:
        wait_instance_stopped.wait(InstanceIds=[wait_instance_id])
    except WaiterError as e:
        sys.exit('---ERROR {}---'.format(e))


if __name__ == '__main__':
    # create a parser to check each command entered
    parser = argparse.ArgumentParser(description='Encrypt EBS volumes attached to EC2 instances')
    required = parser.add_argument_group('required arguments')
    required.add_argument('-i', '--instance', help='File containing instance id\'s to encrypt volume on.', required=True)
    parser.add_argument('-key', '--customer_master_key', help='Customer master key', required=False)
    parser.add_argument('-p', '--profile', help='Customer Profile', required=False)
    args = parser.parse_args()

    # open the passed file, otherwise throw an error
    try:
        instance_file = open(args.instance, "r")
    except Exception as e:
        sys.exit("ERROR {}".format(e))

    # read in all instance id's
    instance_ids = instance_file.readlines()
    for i in range(len(instance_ids)):
        instance_ids[i] = instance_ids[i].rstrip().replace(" ", "")

    # pass in a profile to the session if provided
    if args.profile:
        session = boto3.session.Session(profile_name=args.profile)
    else:
        session = boto3.session.Session()

    # create a new session
    client = session.client('ec2')
    ec2 = session.resource('ec2')

    # create waiters for instances and volumes
    waiter_instance_exists = client.get_waiter('instance_exists')
    waiter_instance_stopped = client.get_waiter('instance_stopped')
    waiter_instance_running = client.get_waiter('instance_running')
    waiter_snapshot_complete = client.get_waiter('snapshot_completed')
    waiter_volume_available = client.get_waiter('volume_available')

    customer_master_key = args.customer_master_key

    for instance_id in instance_ids:
        if instance_id == '':
            continue
        print("\nINSTANCE {}".format(instance_id))
        # check if the instance exists in AWS
        print("---Checking Instance ({})---".format(instance_id))
        instance = ec2.Instance(instance_id)

        try:
            waiter_instance_exists.wait(InstanceIds=[instance_id, ])
        except WaiterError as e:
            print('ERROR: {}'.format(e))
            continue

        if instance.state['Code'] in instance_exit_states:
            print('---ERROR: Instance ({}) is \'{}\', please make instance \'active\'---'.format(instance_id,
                                                                                                 instance.state[
                                                                                                     'Name']))
            print("---Skipping to next instance---")
            continue

        # get all volumes currently attached to the running instance
        volumes = [v for v in instance.volumes.all()]
        # check to see that there is at least one volume
        if len(volumes) != 0:
            processes = []

            # create a process for each volume in the instance
            for index, volume in enumerate(volumes):
                p = multiprocessing.Process(target=main, args=(volume.id, instance_id, customer_master_key, args.profile))
                processes.append(p)
                p.start()

            # wait for all processes to finish
            for process in processes:
                process.join()

        # restart instance if it isn't running
        if instance.state['Code'] in instance_exit_states:
            instance_start(instance, instance_id, waiter_instance_running)

        print("Finished all EBS Volumes for Instance ({})".format(instance_id))

    print("Completed all Instances")
