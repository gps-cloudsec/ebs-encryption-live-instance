# README #

Encrypt all non-encrypted EBS volumes currently attached to all specified running EC2 instances. All volumes attached to an instance are split into separate processes so they can be encrypted in parallel. The EC2 instance ids will be in a .txt file passed into the program specified by the user. 

### Set Up ###

* Dependencies - see requirements.txt

### Run ###

The script goes through each EC2 instance sequentially but does all volumes attached to the instance in parallel through individual processes. Since each volume is independent of another, printing in may be out of order.

If an EC2 instance id is invalid or not currently active, an error will print and the script simply moves onto the next id.  

In the event an EC2 instance has a high number of volumes attached, a volume or snapshot may have to wait 15 seconds before making another request to become available or be created.


* Parameters 
	- '-i' = Required
       -This is the path to the .txt file containing the EC2 instance ids.
		- Each EC2 instance id must be on a separate line
		- Example:
			```
			i-0a0970d2b23d605ce
			i-0a9b34cc9fd6d6201
			i-01fee43efe0992587
			i-0ce49c1545dc41d93
			i-0c480d8f6fc226f43
			```
		
	- '-key' = Optional
		- Customer Master Key (CMK)
	- 'p' = Optional
		- Customer Profile to use
		
		
* To Run the program 
	```
	python EBSEncryptParallel.py -i {file path} -key {CMK} -p {profile}
	```
	- Example with only text file
	```
		python EBSEncryptParallel.py -i ./instance_id.txt	
	```
	- For help
	```
	 python EBSEncryptParallel.py -h
	usage: EBSEncryptParallel.py [-h] -i I [-key KEY] [-p P]

	Encrypt EBS volumes attached to EC2 instances

	optional arguments:
	  -h, --help  show this help message and exit
	  -key KEY    Customer master key
	  -p P        Customer Profile

	required arguments:
  	-i I        File containing instance id's to encrypt volume on.
	```
	
### Example Outputs 
- 3 volumes successfully encrypted
```
	INSTANCE i-0a0970d2b23d605ce
	---Checking Instance (i-0a0970d2b23d605ce)---
	Volume vol-05696dde9fff07d9d
	---Creating Snapshot for Volume (vol-05696dde9fff07d9d)---
	Volume vol-0d1d69666c21c67fb
	---Stopping Instance (i-0a0970d2b23d605ce)---
	Volume vol-0f7bd90d047144422
	---Creating Snapshot for Volume (vol-0f7bd90d047144422)---
	---Trouble creating snapshot. Waiting---
	---Creating Encrypted Snapshot for (snap-09874d47255a94eb6)---
	---Creating Encrypted Snapshot for (snap-0130585d2fb8bfd92)---
	---Creating new encrypted volume for instance i-0a0970d2b23d605ce
	---Detaching current volume vol-0f7bd90d047144422 from instance i-0a0970d2b23d605ce
	---Creating new encrypted volume for instance i-0a0970d2b23d605ce
	---Detaching current volume vol-05696dde9fff07d9d from instance i-0a0970d2b23d605ce
	---Creating Snapshot for Volume (vol-0d1d69666c21c67fb)---
	---Attaching new Encrypted Volume (vol-02bb5f1bb251de886) to Instance (i-0a0970d2b23d605ce)
	---Attaching new Encrypted Volume (vol-07eefaa2c1f054bd3) to Instance (i-0a0970d2b23d605ce)
	---Creating Encrypted Snapshot for (snap-085eb19432caac161)---
	---Creating new encrypted volume for instance i-0a0970d2b23d605ce
	---Detaching current volume vol-0d1d69666c21c67fb from instance i-0a0970d2b23d605ce
	---Attaching new Encrypted Volume (vol-0d87fe3735be0b321) to Instance (i-0a0970d2b23d605ce)
	---Restarting Instance (i-0a0970d2b23d605ce)---
	Finished all EBS Volumes for Instance (i-0a0970d2b23d605ce)
	Completed all Instances
```				

- Instance not active at runtime
```
	INSTANCE i-0ce49c1545dc41d93
	---Checking Instance (i-0ce49c1545dc41d93)---
	---ERROR: Instance (i-0ce49c1545dc41d93) is 'stopped', please make instance 'active'---
	---Skipping to next instance---

	INSTANCE i-0c480d8f6fc226f43
	---Checking Instance (i-0c480d8f6fc226f43)---
	---ERROR: Instance (i-0c480d8f6fc226f43) is 'stopped', please make instance 'active'---
	---Skipping to next instance---
	Completed all Instances
```

		
- Volume already encrypted at runtime
```
	INSTANCE i-0a0970d2b23d605ce
	---Checking Instance (i-0a0970d2b23d605ce)---
	Volume vol-07eefaa2c1f054bd3
	---Volume (vol-07eefaa2c1f054bd3) is already encrypted---
	Volume vol-02bb5f1bb251de886
	---Volume (vol-02bb5f1bb251de886) is already encrypted---
	Volume vol-0d87fe3735be0b321
	---Volume (vol-0d87fe3735be0b321) is already encrypted---
	Finished all EBS Volumes for Instance (i-0a0970d2b23d605ce)
	Completed all Instances
```

- Invalid instance id 
```
	INSTANCE sedgd
	---Checking Instance (sedgd)---
	ERROR: Waiter InstanceExists failed: An error occurred (InvalidInstanceID.Malformed): Invalid id: "sedgd"
```